FROM alpine
WORKDIR /app
# copy the executable from the builder image
COPY ./app .

EXPOSE 8080
ENTRYPOINT ["./app"]